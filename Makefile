# Compiles with gcc by default
all:
	gcc src/main.c -o genre-generator

# Compiles with CLANG instead
clang:
	clang src/main.c -o genre-generator
    
# Compiles with gcc and tests
test:
	gcc src/main.c -o genre-generator
	./genre-generator
	rm genre-generator*
    
# Compiles with clang and tests
test-clang:
	clang src/main.c -o genre-generator
	./genre-generator
	rm genre-generator*

# Removes executable
clean:
	rm genre-generator*