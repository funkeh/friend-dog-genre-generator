/* chikun :: 2014
 * Friend Dog genre generator
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main()
{

    char tmp[1024][32];
    char line[32];
    int matching, dogCount, genreCount, i;
    FILE *file;


    /* Import dogs */
    dogCount = 0;
    file = fopen("txt/dogs.txt", "r");
    while (fscanf(file, "%s", tmp[dogCount]) != EOF) {
        dogCount++;
    }
    fclose(file);

    /* Add dogs to array */
    char dogs[dogCount][32];
    for (i = 0; i < dogCount; i++) {
        strcpy(dogs[i], tmp[i]);
    }


    /* Import genres */
    genreCount = 0;
    file = fopen("txt/genres.txt", "r");
    while (fscanf(file, "%s", tmp[genreCount]) != EOF) {
        genreCount++;
    }
    fclose(file);

    /* Add genres to array */
    char genres[genreCount][32];
    for (i = 0; i < genreCount; i++) {
        strcpy(genres[i], tmp[i]);
    }

    /* Char which will hold user-inputted commands */
    char command;

    /* Print opening title */
    printf("%s\n%s\n",
           "FRIEND DOG GENRE GENERATOR",
           "--------------------------");

    /* Goto for help */
    help:

    /* Print command help */
    printf("%s\n%s\n%s\n%s\n%s\n",
           "Command list:",
           "a   Generate alliterating genre",
           "g   Generate genre",
           "h   Print this help",
           "q   Quit program");


    /* Goto for retyping */
    restart:

    /* Get input */
    printf("> ");

    /* Scan input */
    scanf(" %s", &command);

    /* Don't care about case matching */
    matching = 0;

    switch (command) {

        case 'a':
            matching = 1;

        case 'g':
        {
            /* Seed random number generator for dog */
            srand ((double) clock());

            /* Get random dog */
            char tmpDog[32];
            strcpy(tmpDog, dogs[rand() % dogCount]);


            /* Seed random number generator for genre */
            srand ((double) clock() / 64);

            /* Loop through genres, find matching if required */
            char tmpGenre[32];
            do {
                strcpy(tmpGenre,
                       genres[rand() % genreCount]);
            } while (matching && tmpDog[0] != tmpGenre[0]);


            /* Print data */
            printf("%s\n%s\n%s %s\n",
                  "Your new genre is:",
                  "------------------",
                  tmpDog, tmpGenre);

            break;

        }
        case 'h':

            goto help;
            break;

    }

    /* Loop restarting */
    if (command != 'q') {
        goto restart;
    }

    return 0;

}
